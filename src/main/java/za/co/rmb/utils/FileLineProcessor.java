package za.co.rmb.utils;


import za.co.rmb.entity.Plateau;
import za.co.rmb.entity.Rover;

import java.util.LinkedList;

public class FileLineProcessor {

    private static Integer getIntegerValue(String number) throws Exception {
        int n;
        try {
            n =Integer.valueOf(number);
            if (n <0) {
                throw new Exception("");
            }
        } catch (NumberFormatException nfe) {
            throw new Exception("Coordinates must a number greater than or equal to 0.");
        }

        return n;
    }

    private static Coordinate buildCoordinate(String[] xy) throws Exception {
        if (xy.length < 2) {
            throw new Exception("Invalid coordinates specified.");
        }

        return new Coordinate(getIntegerValue(xy[0]), getIntegerValue(xy[1]));
    }

    public static Plateau buildPlateau(String coords) throws Exception {
        String[] xy = coords.split(" ");

        if (xy.length != 2){
            throw new Exception("Invalid plateau size.");
        }

        return new Plateau(buildCoordinate(xy));
    }

    public static Rover buildRover(String coords, String commands,Plateau plateau) throws Exception {
        String[] xyd = coords.split(" ");
        if (xyd.length != 3){
            throw new Exception("Invalid rover coordinates and direction.");
        }
        Coordinate coordinate = buildCoordinate(xyd);

        Direction direction;
        try {
            direction= Direction.valueOf(xyd[2]);
        } catch (Exception e) {
            throw new Exception("Invalid direction specified.");
        }

        if (commands == null || commands.isEmpty()) {
            throw new Exception("No commands found.");
        }

        LinkedList<Command> commandlist = new LinkedList<>();

        for(int i=0; i < commands.length(); i++) {
            // i guess if a command is not valid, it shouldn't stop the rover from performing following commands
            try {
                commandlist.add(Command.valueOf(""+commands.charAt(i)));
            } catch (Exception e) {}
        }

        return new Rover(coordinate, direction, commandlist, plateau);
    }
}
