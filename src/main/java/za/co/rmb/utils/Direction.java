package za.co.rmb.utils;


public enum Direction {
    N,S,W,E;

    public Integer getOrdinal() {
        switch (this){
            case N: return 0;
            case E: return 1;
            case S: return 2;
            case W: return 3;
            default:
                return null;

        }
    }

    public static Direction forOrdinal(Integer i) {
        switch (i){
            case 0: return N;
            case 1: return E;
            case 2: return S;
            case 3: return W;
            default:
                return null;

        }
    }
}
