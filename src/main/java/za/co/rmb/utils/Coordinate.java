package za.co.rmb.utils;

import lombok.*;

/**
 * Created by bhavic on 1/26/2017.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Coordinate {
    private Integer x, y;

    public void updateCoordinate(int addX, int addY, Coordinate maxXY) {
        x +=addX;
        y+= addY;

        if (x < 0) {
            x = 0;
        }
        if (y < 0) {
            y = 0;
        }
        if (x > maxXY.getX()) {
            x = maxXY.getX();
        }
        if (y > maxXY.getY()) {
            y = maxXY.getY();
        }
    }
}
