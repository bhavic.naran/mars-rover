package za.co.rmb;

import org.apache.commons.io.IOUtils;
import za.co.rmb.entity.Plateau;
import za.co.rmb.entity.Rover;
import za.co.rmb.utils.FileLineProcessor;

import java.io.FileInputStream;
import java.util.List;

public class Runner {

    public static void main(String[] args) {
        // read and validate file
        if (args == null || args.length != 1) {
            System.out.println("Usage: [java] [Runner] [filename.txt]");
        }

        try (FileInputStream fis = new FileInputStream(args[0]))  {
            List lines = IOUtils.readLines(fis);

            // need at least 3 lines in order for this to work
            if (lines == null || lines.size() < 3) {
                throw new Exception("Invalid input file.");
            }

            //handle plateau
            Plateau plateau = FileLineProcessor.buildPlateau((String)lines.get(0));

            // rover information is split on 2 lines
            // 1) coordinates and direction
            // 2) commands
            for (int i=1,j =0; i< lines.size(); i+=2) {
                try {
                    Rover rover = FileLineProcessor.buildRover((String)lines.get(i),(String)lines.get(i+1), plateau);
                    rover.processCommand();
                    System.out.printf("%d %d %s\n", rover.getCurrentXY().getX(), rover.getCurrentXY().getY(), rover.getDirection());
                } catch (Exception e) {
                    System.out.println("Error on rover:" + ((i+1)/2) + " -" + e.getMessage());
                }

            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
