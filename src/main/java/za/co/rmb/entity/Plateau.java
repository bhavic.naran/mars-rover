package za.co.rmb.entity;

import lombok.*;
import za.co.rmb.utils.Coordinate;

/**
 * Created by bhavic on 1/26/2017.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class Plateau {
    private Coordinate maxXY;
}
