package za.co.rmb.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import za.co.rmb.utils.Command;
import za.co.rmb.utils.Coordinate;
import za.co.rmb.utils.Direction;

import java.util.LinkedList;

/**
 * Created by bhavic on 1/26/2017.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Rover {
    private Coordinate currentXY;
    private Direction direction;
    private LinkedList<Command> commandList;
    private Plateau mars;

    private void handleDirectionChange(Command command) {
        if (Command.M.equals(command)) {
            return;
        }

        int i = direction.getOrdinal();
        if (command.equals(Command.L)) {
            i --;
        }
        if (command.equals(Command.R)) {
            i ++;
        }

        if (i < 0) {
            i = 4 + i;
        }

        i = i % 4;

        direction = Direction.forOrdinal(i);
    }

    private void handleMovement(Command command) {
        if (!command.equals(Command.M)) {
            return;
        }

        switch (direction){
            case N: currentXY.updateCoordinate(0, 1, getMars().getMaxXY()); break;
            case E: currentXY.updateCoordinate(1, 0, getMars().getMaxXY()); break;
            case S: currentXY.updateCoordinate(0, -1, getMars().getMaxXY()); break;
            case W: currentXY.updateCoordinate(-1, 0, getMars().getMaxXY()); break;
            default: break;
        }
    }

    public void processCommand() {
        for (Command command : commandList) {
            handleDirectionChange(command);
            handleMovement(command);
        }
    }
}
